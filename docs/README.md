# Introduction

This is a small website that contains tips & recommendations on how to work best with RSpec for developers who come from a javascript background. The examples provided assume previous experience with [vue-test-tools](https://vue-test-utils.vuejs.org/) and [jest](https://jestjs.io/)

## Finders and Matchers

### Finders

### `find` -> `find`

To find an element inside a wrapper with vue-test-tools, you use the `find` function

```javascript
const element = wrapper.find('#some-element');
```

The equivalent in rspec is `find`

```ruby 
element = find('#some-element')
```

### `findAll` -> `all`

To find all of the elements that match a specific class or attribute, you use `findAll`

```javascript
const elements = wrapper.findAll('.multiple-elements');
```

The equivalent in rspec is `all`

```ruby 
elements = all('.multiple-elements')
```

### find_button

Just like `find` but this is specific to `<button>` elements,

```ruby
find_button(name, options)
```

Where

- `name`: Can be the ID, name, value, title, text, content or even the `alt` attribute
- `options`: The most notable option is the ability to look for a `disabled` button, more information can be found [here](https://rubydoc.info/github/jnicklas/capybara/master/Capybara%2FNode%2FFinders:find_button)

### fill_in

Just like `find` but this is specific to `<input>` or `<textarea>` elements,

```ruby
fill_in(name, options)
```

Where

- `name`: Can be the ID, name, value, title, text, content or even the `alt` attribute
- `options`: The most notable option is the ability to look for a `disabled` button, more information can be found [here](https://rubydoc.info/github/jnicklas/capybara/master/Capybara/Node/Actions:fill_in)

### Tips

When working in Jest the convention is to test components in isolation, whereas in RSpec the type of tests you create test how your components integrate (also known as feature tests in RSpec) with the page the component lives in. Please keep in mind the following

#### Try to reduce the scope of the entire page

Sometimes a page will have multiple buttons that might have the same element ID or even name, some RSpec matchers will get confused as some functions expect only to get one element in return, to prevent
that you can use `page.within` to limit the search scope of a finder function to just the section you want to test

```ruby
page.within '#section-one' do
  click_button('#save-button');
end
```

#### RSpec finders use the same syntax as `querySelector`

Just like in javascript's `document.querySelector` API, `find` and `all` follow the same selector syntax

##### By ID

```ruby
find('#element')
```

##### By Class

```ruby
find('.element')
```

##### By Attribute

```ruby
find('.element[name="one"]')
```

##### With pseudo selectors

```ruby
find('.element:first-child')
```

#### Hidden elements can't be found

If the element you're looking contains any one of the following

```CSS
#element {
  visibility: hidden;
  display: none;
}
```

The RSpec finders won't be able to find it and you will get an error

### Enabling javascript in a test

By default all feature specs don't run with javascript enabled as to help make the test run time lower. To enable javascript add the `:js` attribute in the `describe` block of your test

```ruby
RSpec.describe 'javascript enabled test', :js do
  ...
end
```

### Using `yield`

`yield` is a ruby keyword that allows you to inject code in a block of code, replacing the `yield` keyword
with whatever code you want to replace, this is particularly useful when dealing with expectations that
need some prior setup, e.g.

```ruby
def setup_script
  fill('#some-variable', 'expected text')

  yield
end

# in your it block
it 'should have some text in the input' do
  setup_script do
    # This will already run the previous `fill` function

    expect(find('#some-variable').value).to be('expected text')
  end
end
```

## Ruby on Rails conventions

### File locations

If you want to edit a previous file to add more test coverage or create a new integration test, RSpec configures the `spec/features` folder by default as to where all the feature specs will live

### Debugging with Google Chrome in realtime*

If you want to run your tests and see the interactions in real time, you can disable the flag `CHROME_HEADLESS`

```shell
CHROME_HEADLESS=0 bundle exec rspec spec/features/cool_test.rb
```

This will only work with tests that have [javascript enabled](#enabling-javascript-in-a-test)

### Waiting for requests*

In RSpec asynchronous requests need to finish executing before expectations can be done, for that 
`wait_for_requests` needs to be used

```ruby
it 'does something as expected' do
  find('#button').click

  wait_for_request

  expect(page).to have_content('Action finished succesfully')
end
```

### `let` and `create`

To create data that is persisted for each run and gets teared down after all of the specs have passed, in RSpec you use [factories](https://github.com/thoughtbot/factory_bot/blob/master/GETTING_STARTED.md)

```ruby
let(:user) { create(:user) }
```

## Creating and tearing down a test*

Let's create a simple test block by block

1. Create a `describe` block

```javascript
describe('javascript test', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallowMount(Component, {})
  });

  afterEach(() => {
    wrapper.destroy();
  });
});
```

```ruby
require 'spec_helper'

RSpec.describe 'RSpec feature test' do
  let(:user) { create(:user) }

  before do
    visit users_path
  end
end
```

- On RSpec we don't need to setup specific components, but rather factories that setup the necessary information for a page to be able to be rendered.
- The factories are setup and teared down automatically.
- The `beforeEach` and `afterEach` equivalents are `before` and `after`.

2. Create a new `it` block

```javascript
describe('javascript test', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallowMount(Component, {})
  });

  afterEach(() => {
    wrapper.destroy();
  });

  it('does something as expected', () => {
    expect(wrapper.attributes('disabled')).toBe(false);
  });
});
```

```ruby
require 'spec_helper'

RSpec.describe 'RSpec feature test' do
  let(:user) { create(:user) }

  before do
    visit users_path
  end

  it 'does something as expected' do
    button = find_button('save', { disabled: true })

    expect(button).not_to be_nil
  end
end
```

- With Jest, our wrapper only contains the button we want to test, whereas RSpec contains the entire page so finding the button is necessary.
- On javascript `null` and `undefined` represent elements that have not been initialized or that don't exist, on ruby `null` and `nil` accomplish the same thing

`* This only works for the GitLab project`
